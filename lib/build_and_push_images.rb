require 'yaml'
require 'aws-sdk-ecr'
require 'erb'

class BindConfig
  def initialize region, access_key_id, secret_access_key, docker_containers_namespace
    @region = region
    @access_key_id = access_key_id
    @secret_access_key = secret_access_key
    @docker_containers_namespace = docker_containers_namespace
  end

  def get_binding
    return binding()
  end
end

begin

  puts 'Setting up aws credentials '
  puts 'Provide the following info: '
  puts "AWS Region : "
  region = gets.strip
  puts "AWS Access Key Id : "
  access_key_id = gets.strip
  puts "AWS Secret Access Key : "
  secret_access_key = gets.strip
  puts "Namespace for cronus containers (default: cronus-containers) : "
  docker_containers_namespace = gets.strip
  puts "Configuring"

  @settings = YAML.load_file(File.join(Dir.pwd, 'config', 'settings.yml'))

  @ECR_CONFIG = @settings['ecr_settings']

  region ||= @ECR_CONFIG['region']
  access_key_id ||= @ECR_CONFIG['access_key_id']
  secret_access_key ||= @ECR_CONFIG['secret_access_key']
  docker_containers_namespace ||= @ECR_CONFIG['docker_containers_namespace']

  File.open(File.join(Dir.pwd, 'config', 'settings.yml'), 'w') do |f|
    f.write(ERB.new(File.read(File.join(Dir.pwd, 'config', 'settings.yml.erb'))).result(BindConfig.new(region, access_key_id, secret_access_key, docker_containers_namespace).get_binding))
  end

  ENV['AWS_ACCESS_KEY_ID'] = access_key_id
  ENV['AWS_SECRET_ACCESS_KEY'] = secret_access_key
  ENV['AWS_DEFAULT_REGION'] = region

  @settings = YAML.load_file(File.join(Dir.pwd, 'config', 'settings.yml'))

  @ECR_CONFIG = @settings['ecr_settings']

  Aws.config.update(
    region: @ECR_CONFIG['region'],
    access_key_id: @ECR_CONFIG['access_key_id'],
    secret_access_key: @ECR_CONFIG['secret_access_key']
  )

  docker_namespace = @ECR_CONFIG['docker_containers_namespace']
  ecr_client = Aws::ECR::Client.new
  remote_repositories = []
  described_repositories = ecr_client.describe_repositories
  remote_repositories = described_repositories.repositories unless described_repositories.nil?
  remote_repositories = remote_repositories.map{|repo| repo.repository_name} unless remote_repositories.empty?

  source_directory = File.join(Dir.pwd, 'src', 'alpine')

  system('service docker start')
  get_login_cmd = `aws ecr get-login --no-include-email --region #{@ECR_CONFIG['region']}`
  system("#{get_login_cmd}")

  languages_config = YAML.load_file(File.join(Dir.pwd, 'config', 'versions.yml'))['languages']
  languages_config.each do |language_config|
    language = language_config['name']
    remote_repo = "#{docker_namespace}/#{language}"

    unless remote_repositories.include?(remote_repo)
      puts "#{language} repo not found on remote, Creating"
      ecr_client.create_repository(repository_name: remote_repo)
    end

    language_config['versions'].each do |lang_version|
      puts "#"*40
      puts "#{language} | #{lang_version['version']}"

      if lang_version['active']

        system("docker stop $(docker ps -a -q)")
        system("docker rm $(docker ps -a -q)")
        system("docker rmi -f $(docker images -a -q)")

        image_name = [language, lang_version['version']].join(':')
        repository_uri = ecr_client.describe_repositories.repositories.select{|repo| repo.repository_name == remote_repo}.first.repository_uri
        docker_image = "#{repository_uri}:#{lang_version['version']}"
        dockerfile_directory = File.join(source_directory, language, lang_version['version'])
        docker_build_cmd = "docker build -t #{docker_image} #{dockerfile_directory}"
        docker_push_cmd = "docker push #{docker_image}"

        puts "---Building image---"
        puts docker_build_cmd
        system docker_build_cmd
        puts "---Built image---"

        puts '---Pushing image---'
        puts docker_push_cmd
        system docker_push_cmd
        puts "---Pushed image---"
      end
      puts "#"*40
    end
  end
  puts 'All updated images pushed'
rescue StandardError => e
  puts 'Error'
  puts e
end
