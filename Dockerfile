FROM ruby:2.4
ADD config home/config
ADD lib home/lib
ADD src home/src
ADD Gemfile home/Gemfile
ADD Gemfile.lock home/Gemfile.lock
RUN \
  mkdir -p /aws && \
  apt-get update && \
  apt-get install -y python-pip python-dev libpython-dev && \
  pip install awscli && \
  apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
  curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add - && \
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" && \
  apt-get update && \
  apt-get install -y docker-ce && \
  usermod -aG docker $(whoami)
RUN \
  cp /home/config/versions.yml.example home/config/versions.yml && \
  cp /home/config/settings.yml.example home/config/settings.yml
RUN \
  cd home && bundle
WORKDIR /home
# MAINTAINER: AKASH SRIVASTAVA akashsrvstv@yahoo.in
# do `docker run --privileged -it cronus-containers:latest ruby lib/build_and_push_images.rb`
